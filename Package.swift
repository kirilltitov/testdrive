// swift-tools-version:4.1

import PackageDescription

let package = Package(
    name: "TestDrive",
    dependencies: [
        .package(url: "https://github.com/apple/swift-nio", .branch("master")),
        .package(url: "https://github.com/IBM-Swift/BlueSocket", .upToNextMajor(from: "1.0.0")),
    ],
    targets: [
        .target(
            name: "TestDrive",
            dependencies: ["NIO", "Socket"]),
    ]
)
