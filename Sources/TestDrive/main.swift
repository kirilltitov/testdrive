import Foundation
import Socket
import NIO

typealias Byte = UInt8
typealias Bytes = [Byte]

let host = "127.0.0.1"
let port = 1711

internal extension ByteBufferAllocator {
    func allocateBuffer(from string: String, encoding: String.Encoding = .utf8) -> ByteBuffer {
        let bytes = Bytes(string.utf8)
        var buf = self.buffer(capacity: bytes.count)
        buf.write(bytes: bytes)
        return buf
    }
    
    func allocateBuffer(from bytes: Bytes) -> ByteBuffer {
        var buf = self.buffer(capacity: bytes.count)
        buf.write(bytes: bytes)
        return buf
    }
    
    func buffer(capacity: UInt8) -> ByteBuffer {
        return self.buffer(capacity: Int(capacity))
    }
}

func send(payload: String, to host: String, port: Int) -> String {
    let connection: Socket = try! Socket.create(family: .inet, type: .stream, proto: .tcp)
    try! connection.connect(to: host, port: Int32(port))
    let _ = try! connection.write(from: payload)
    var output = Data()
    // endless wait
    let _ = try! connection.read(into: &output)
    return String(data: output, encoding: .ascii)!
}

internal final class ServerHandler: ChannelInboundHandler {
    public typealias InboundIn = ByteBuffer
    public typealias OutboundOut = ByteBuffer
    
    public init() {}
    
    public func channelRead(ctx: ChannelHandlerContext, data: NIOAny) {
        var input = self.unwrapInboundIn(data)
        let inputString = String(
            bytes: input.readBytes(length: input.readableBytes)!,
            encoding: .ascii
        )!.trimmingCharacters(in: .whitespacesAndNewlines)
        let internalURI = "internal_method"
        let response: String
        print("Server responding on '\(inputString)'")
        switch inputString {
        case "normal":
            response = "normal response"
        case "deadlock":
            // request self
            let internalResponse = TestDrive.send(payload: internalURI, to: host, port: port)
            // this won't happen :(
            response = "internal response: \(internalResponse)"
        case internalURI:
            response = "\(internalURI) ok"
        default:
            response = "unknown method"
        }
        print("Response is '\(response)'")
        self.send(response, to: ctx)
    }
    
    private func send(_ byteBuffer: ByteBuffer, to ctx: ChannelHandlerContext, close: Bool = true) -> Void {
        ctx.writeAndFlush(self.wrapOutboundOut(byteBuffer), promise: nil)
        if close == true {
            ctx.close(promise: nil)
        }
        return
    }
    
    private func send(_ bytes: Bytes, to ctx: ChannelHandlerContext, close: Bool = true) -> Void {
        return self.send(ctx.channel.allocator.allocateBuffer(from: bytes), to: ctx, close: close)
    }
    
    private func send(_ string: String, to ctx: ChannelHandlerContext, close: Bool = true) -> Void {
        return self.send(ctx.channel.allocator.allocateBuffer(from: string), to: ctx, close: close)
    }
    
    public func channelReadComplete(ctx: ChannelHandlerContext) {
        ctx.flush()
    }
    
    public func errorCaught(ctx: ChannelHandlerContext, error: Error) {
        print("error: ", error)
        ctx.close(promise: nil)
    }
}

do {
    let group = MultiThreadedEventLoopGroup(numberOfThreads: 1)
    let bootstrap = ServerBootstrap(group: group)
        .serverChannelOption(ChannelOptions.backlog, value: 256)
        .serverChannelOption(ChannelOptions.socket(SocketOptionLevel(SOL_SOCKET), SO_REUSEADDR), value: 1)
        
        .childChannelInitializer { channel in
            channel.pipeline.add(handler: BackPressureHandler()).then { _ in
                channel.pipeline.add(handler: ServerHandler())
            }
        }
        
        .childChannelOption(ChannelOptions.socket(IPPROTO_TCP, TCP_NODELAY), value: 1)
        .childChannelOption(ChannelOptions.socket(SocketOptionLevel(SOL_SOCKET), SO_REUSEADDR), value: 1)
        .childChannelOption(ChannelOptions.maxMessagesPerRead, value: 64)
        .childChannelOption(ChannelOptions.recvAllocator, value: AdaptiveRecvByteBufferAllocator())
    
    let channel = try bootstrap.bind(host: "127.0.0.1", port: 1711).wait()
    print("Server started")
    try channel.closeFuture.wait()
} catch {
    dump(error)
}
